## AMB Defaults - Set defaults when creating a project
#

from trac.core import *
from trac.env import IEnvironmentSetupParticipant

import logging

STATEMENTS = (\
    ("DELETE FROM milestone", ()),
    ("INSERT INTO milestone (name, due, completed) VALUES (%s,%s,%s)", ('1.0 Produccion', 0, 0)),
    ("DELETE FROM version", ()),
    ("INSERT INTO version (name, time)  VALUES (%s,%s)", ('0.99', 0)),
    ("DELETE FROM permission WHERE username = %s AND action = %s", ('anonymous', 'WIKI_CREATE')),
    ("DELETE FROM permission WHERE username = %s AND action = %s", ('anonymous', 'WIKI_MODIFY')),
    ("DELETE FROM permission WHERE username = %s AND action = %s", ('anonymous', 'TICKET_CREATE')),
    ("DELETE FROM permission WHERE username = %s AND action = %s", ('anonymous', 'TICKET_MODIFY')),
)

class AMBDefaultsPlugin(Component):
    """ Adds Sets some data on project setup """
    implements(IEnvironmentSetupParticipant)
    
    def environment_created(self):
        db = self.env.get_db_cnx()
        cursor = db.cursor()
        print "AMB Defaults, executing statements"
        for statement in STATEMENTS:
            print "Executing: " + statement[0] % tuple(["'%s'" % x for x in statement[1]])
            cursor.execute(statement[0], statement[1])
        
        db.commit()
    
    def environment_needs_upgrade(self, db):
        return False;
    
    def upgrade_environment(self, db):
        pass
        
