from setuptools import setup

PACKAGE = 'tracambdefaults'
VERSION = '0.1'

setup(name=PACKAGE,
      version=VERSION,
      packages=['tracambdefaults'],
      author='Alvaro J. Iradier',
      url="",
      license='BSD',
      entry_points = {'trac.plugins': ['tracambdefaults = tracambdefaults']})
