"""
This script will change the SVN repository, from OLD_SVN_PATH to NEW_SVN_PATH,
in all the Trac environments found in the directory SEARCH_PATH
"""

SEARCH_PATH = "c:/path/to/environments/folder" 
OLD_SVN_PATH = "c:/path/to/old/svn/repo"
NEW_SVN_PATH = "c:/path/to/new/svn/repo"

#####################################################################

from trac.core import *
from trac.env import open_environment

import os

old_path = os.path.normpath(OLD_SVN_PATH)
old_path = os.path.normcase(old_path)

new_path = os.path.normpath(NEW_SVN_PATH)
new_path = os.path.normcase(new_path)

for project in os.listdir(SEARCH_PATH):

    project_path = os.path.join(SEARCH_PATH, project)
    
    if not os.path.isdir( project_path ):
        continue
    
    try:
        env = open_environment(project_path)
    except:
        continue
        
    curr_path = env.config.get('trac', 'repository_dir')
    curr_path = os.path.normpath(curr_path)
    curr_path = os.path.normcase(curr_path)
    
    if os.path.commonprefix([old_path, curr_path]) == old_path:
        print "Updating project %s" % project
        new_curr_path = new_path + curr_path[len(old_path):]
        print "  %s -> %s" % (curr_path, new_curr_path)
        env.config.set('trac', 'repository_dir', new_curr_path)
        env.config.save()
        try:
            env.get_repository().sync()
        except Exception, ex:
            print "  Error syncing repository"
            print ex
