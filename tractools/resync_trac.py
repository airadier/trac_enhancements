"""
This script will apply the 'trac-admin resync' command to all the 
Trac Environments in the ENVS_DIR directory. The TRAC_ADMIN command
must be set-up
"""

ENVS_DIR = "c:\\path\\to\\trac\\envs_directory"
TRAC_ADMIN = "c:\\python25\\scripts\\trac-admin.exe"

#####################################################################

from glob import glob
import os.path
import os
import commands

projects = glob(os.path.join(ENVS_DIR, "*"))

for project in projects:
    conf_file = os.path.join(project,"conf", "trac.ini")
    if os.path.exists(conf_file):
        print "Resyncing %s" % project
        print "-----------------------------------------------"
    
    
    
        o = os.system("%s %s resync" % (TRAC_ADMIN, project))
        print o
        
        print ""
        