"""
This script will apply the 'trac-admin upgrade' command to all the 
Trac Environments in the ENVS_DIR directory. The TRAC_ADMIN command
must be set-up
"""

ENVS_DIR = "c:\\path\\to\\trac\\envs_directory"
TRAC_ADMIN = "c:\\python25\\scripts\\trac-admin.exe"
#TRAC_SETTINGS = "c:\\trac_envs\\trac.ini"

#####################################################################

from glob import glob
import os.path
import os
import commands

projects = glob(os.path.join(ENVS_DIR, "*"))

for project in projects:
    conf_file = os.path.join(project,"conf", "trac.ini")
    if os.path.exists(conf_file):
        print "Upgrading %s" % project
        print "-----------------------------------------------"
    
        o = os.system("%s %s upgrade" % (TRAC_ADMIN, project))
        print o
        o = os.system("%s %s wiki upgrade" % (TRAC_ADMIN, project))
        print o
        
        print ""
        
        #f = open(conf_file, "a")
        #f.write("\n[inherit]\n")
        #f.write("file=%s\n\n" % TRAC_SETTINGS)
        #f.close()
