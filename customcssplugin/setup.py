from setuptools import setup

PACKAGE = 'customcss'
VERSION = '0.1'

setup(name=PACKAGE,
      version=VERSION,
      packages=['customcss'],
      author='Alvaro J. Iradier',
      url="http://www.trac-hacks.org/wiki/CustomCSS",
      license='BSD',
      entry_points = {'trac.plugins': ['customcss = customcss']})
