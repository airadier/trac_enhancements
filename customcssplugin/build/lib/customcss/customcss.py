## AMB CustomCSS - Add a custom CSS stylesheet on every requset

from trac.core import *
from trac.web.api import IRequestFilter
from trac.web.chrome import add_stylesheet

class CustomCSSPluginCS(Component):
    """ Search the source repository. """
    implements(IRequestFilter)

    def pre_process_request(self, req, handler):
        return handler
        
    
    def post_process_request(self, req, template, content_type):
        css = self.env.config.get('project', 'customcss', None)
        self.env.log.debug("Using custom CSS: %s" % css )
        if css:
            add_stylesheet(req, css)
        
        return (template, content_type)


class CustomCSSPluginGenshi(Component):
    """ Search the source repository. """
    implements(IRequestFilter)

    def pre_process_request(self, req, handler):
        return handler
            
    def post_process_request(self, req, template, data, content_type):
        css = self.env.config.get('project', 'customcss', None)
        self.env.log.debug("Using custom CSS: %s" % css )
        if css:
            add_stylesheet(req, css)
        
        return (template, data, content_type)
