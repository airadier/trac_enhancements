# -*- coding: utf-8 -*-
"""
AMB Project Manager.
Shared auth Module
"""
import time

## trac imports
import acct_mgr.web_ui
from trac.env import Environment, open_environment
from trac.core import *
from trac.config import BoolOption
from trac.web import IAuthenticator, IRequestFilter, auth
from sharedsettings import get_master_env
from utils import wrapfunc

class SharedAuth(Component):
    
    implements(IRequestFilter)
    implements(IAuthenticator)
    
    def __init__(self):
        #When initing, replace _do_login and _do_logout with customized versions
        #Note these functions cannot be methods of SharedAuth, because there is a SharedAuth instance
        #for every environment, so wrapping would make an infinite recursion        
        wrapfunc(auth.LoginModule, "_do_logout", _shared_do_logout)
        self.env.log.info("ProjectManager SharedAuth replaced LoginModule._do_logout")
        wrapfunc(acct_mgr.web_ui.LoginModule, "_distribute_cookie", _shared_distribute_cookie)
        self.env.log.info("ProjectManager SharedAuth replaced Account Manager LoginModule._distribute_cookie")

    # IRequestFilter methods. Do nothing, just make sure SharedAuth is instantiated
    def pre_process_request(self, req, handler):
        return handler
    
    def post_process_request(self, req, template, data, content_type):
        return template, data, content_type    
    
    # IAuthenticator methods
    def authenticate(self, req):
        if req.incookie.has_key('trac_auth'):
            #Try to authenticate in the master environment
            self.log.debug("Trying to authenticate in master environment")
            env = get_master_env(self.env)            
            remote_user = auth.LoginModule(env).authenticate(req)
            if remote_user:
                self.update_cookie(req, remote_user)
                if req.outcookie.has_key('trac_auth'):
                    req.outcookie['trac_auth'] = req.incookie['trac_auth']
                    if req.outcookie['trac_auth'].has_key('expires'):
                        del req.outcookie['trac_auth']['expires']
            return remote_user
        return None

    def update_cookie(self, req, remote_user):
        trac_auth = req.incookie['trac_auth'].value
        db = self.env.get_db_cnx()
        cursor = db.cursor()
        cursor.execute("""
            DELETE FROM auth_cookie
            WHERE  cookie=%s
            """, (trac_auth,))
        cursor.execute("""
            INSERT INTO auth_cookie
                   (cookie,name,ipnr,time)
            VALUES (%s,%s,%s,%s)
            """, (trac_auth, remote_user, req.remote_addr,
                  int(time.time())))
        self.log.debug("update_cookie: %s %s %s", trac_auth, remote_user, req.remote_addr)
        db.commit()

def _shared_do_logout(original_callable, the_class, req, *args, **kwargs):   

    #Call the original _do_logout
    original_callable(the_class, req, *args, **kwargs)

    child_env = the_class.env
    env = get_master_env(child_env)    
    if env.path != child_env.path:
        #Instantiate a trac LoginModule using the master environment, and login on it
        auth.LoginModule(env)._do_logout(req)
    
    auth_path = child_env.config.get('projectmanager', 'auth_cookie_path', '/')
    child_env.log.debug("Changing auth cookie path to %s" % auth_path)
    if req.outcookie.has_key('trac_auth'): 
        req.outcookie['trac_auth']['path'] = auth_path

def _shared_distribute_cookie(original_callable, the_class, req, trac_auth, *args, **kwargs):   

    #En lugar de distribuir a todos los proyectos, distribuir sólo al proyecto índice

    #Call the original _do_logout
    #original_callable(the_class, req, trac_auth, *args, **kwargs)

    child_env = the_class.env

    child_env.log.debug("shared_distribute_cookie: called")

    env = get_master_env(child_env)    
    
    if env.path != child_env.path:
        child_env.log.debug("shared_distribute_cookie: Copying cookie to master project")
        db = env.get_db_cnx()
        cursor = db.cursor()
        cursor.execute("""
            DELETE FROM auth_cookie
            WHERE  cookie=%s
            """, (trac_auth,))
        cursor.execute("""
            INSERT INTO auth_cookie
                   (cookie,name,ipnr,time)
            VALUES (%s,%s,%s,%s)
            """, (trac_auth, req.remote_user, req.remote_addr,
                  int(time.time())))
        db.commit()
            

