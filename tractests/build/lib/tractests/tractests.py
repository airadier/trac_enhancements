# This plugin is used to create and run a set of tests
# Author: MAC


from trac.core import *
from trac.wiki.macros import WikiMacroBase
from trac.wiki.formatter import format_to_html, format_to_oneliner
from trac.wiki.model import WikiPage
from trac.mimeview.api import Context
from trac.resource import Resource
from trac.util.text import unquote, to_unicode
from trac.web import IRequestHandler
from trac.web.chrome import Chrome, add_stylesheet, ITemplateProvider
from trac.util.html import html
from pkg_resources import resource_filename
from trac.util import Markup
from trac.wiki.api import WikiSystem
import StringIO

from trac.attachment import Attachment
from datetime import datetime
import itertools

from glob import glob
import re
import os.path
import os
import sys
import time


__all__ = ['ParseTestFileMacro', 'CreateTestPageMacro', 'TestSuiteTemplateMacro']

def render_template_table(env, req, sections, title, create_mode, page_url=None, page_list=[]):
    add_stylesheet(req, 'tractests/css/testtable.css')    
    #For Genshi, when used, return a 3 element tuple            
    return Chrome(env).render_template(req, 'templatetable.html',
                                            {'cycle': itertools.cycle,
                                             'sections': sections,
                                             'title': title,
                                             'create_mode': create_mode,
                                             'page_url': page_url,
                                             'page_list': page_list,
                                             'macro_id': req.macro_id},
                                             'text/html', False)

def parse_tests_file(env, page, attachment_name):

    tests = []
    try:
        attachment = Attachment(env,
                                'wiki',
                                page,
                                filename=attachment_name, db=env. get_db_cnx())
    except:
        return []

    fd = attachment.open()
    for line in fd.readlines():
        
        line = line.decode('utf-8').strip()
        
        parts = [x.strip('"').strip() for x in line.split(';')]
        tests.append([parts[0], parts[1]])

    fd.close()
            
    return tests

def add_test_to_file(env, req, page, attachment_name, test_data):
        
    data = StringIO.StringIO()
    desc = ''
    try:
        attachment = Attachment(env,
                                'wiki',
                                page,
                                filename=attachment_name, db=env. get_db_cnx())
        
        desc = attachment.description
        
        fd = attachment.open()
        lines = fd.read().splitlines()
        fd.close()
        attachment.delete()
    except:
        pass   

    newlines = []
    for line in lines:
        parts = [x.strip('"').strip() for x in line.split(';')]
        newlines.append(";".join(parts))
    
    newlines.append(";".join(test_data).encode('utf-8'))
    data.write("\n".join(newlines))
    
    size = data.tell()
    data.seek(0)
    
    attachment = Attachment(env, 'wiki', page)
    attachment.description = desc
    attachment.author = req.authname   
    attachment.insert(attachment_name, data, size)
    
    data.close()    


def create_tests_page(env, req, page_name, sections, version='?', targetname='results.txt'):
    
    page = WikiPage(env, page_name)
    
    page.text = u"= %s =\n[[BR]]\nVersion: '''%s'''\n[[BR]]\nAuthor: '''%s'''\n[[BR]]\nDate: '''%s'''\n[[BR]]\n" % (page_name, version, req.authname, datetime.now().strftime('%d/%m/%y %H:%M:%S'))
    page.text = page.text + u"[[ParseTestFile(%s)]]\n[[BR]] " % targetname
    
    fout = StringIO.StringIO()
    fout.write(u"Id;Name;Description;Status;Date and Time;Comments;\n".encode('utf-8'))

    index = 1

    for section_title, tests, section_file in sections:

        if section_title:
            fout.write(("#%s\n" % section_title).encode('utf-8'))
        else:
            fout.write("#\n")

        for test in tests:
            testid = u"%05d" % index
            if len(test) < 3 or parts[2] != 'S': #skip tests with status = 'S'
                index = index + 1
                strdata = u"%s;%s;%s;P;%s;;\n" % (testid, to_unicode(test[0]), to_unicode(test[1]), datetime.now().strftime('%d/%m/%y %H:%M:%S'))
            else:
                strdata = u";%s;%s;;;;\n" % (to_unicode(test[0]), to_unicode(test[1]))

            fout.write(strdata.encode('utf-8'))

    #fout.seek(0, 2) # seek to end of file
    size = fout.tell()
    fout.seek(0)

    page.save(req.authname, 'New test page from template' , req.remote_addr)

    attachment = Attachment(env, 'wiki', page_name)
    attachment.description = "Test Results"
    attachment.author = req.authname   
    attachment.insert(targetname, fout, size)
    fout.close() 


class ParseTestFileMacro(WikiMacroBase):
    """
    Create an HTML table with content of attached file. Attached file is a CSV file.
    
    Example:
    {{{ 
        [[ParseTestFile()]]       # uses default filename resultados.txt to open attached file
        [[ParseTestFile(Foo)]]    # uses Foo filename to open attached file
    }}}
    """        
        
    implements(ITemplateProvider)
    
    ##################################
    ## ITemplateProvider

    def get_htdocs_dirs(self):
        """
        Return the absolute path of a directory containing additional
        static resources (such as images, style sheets, etc).
        """
        return [('tractests', resource_filename(__name__, 'htdocs'))]

    def get_templates_dirs(self):
        """
        Return the absolute path of the directory containing the provided
        ClearSilver templates.
        """
        return [resource_filename(__name__, 'templates')]

    ##################################
    
    def expand_macro(self, formatter, name, args=''):

        try:
            macro_id = formatter.req.macro_id + 1
        except:
            macro_id = 0
        formatter.req.macro_id = macro_id 

        try:
            id_ok = int(formatter.req.args.get('macro_id', None)) == macro_id
        except:
            id_ok = False
        
        try:
            args = [x.strip() for x in args.split(',')]
            input_filename = args[0]
        except:
            input_filename = ''
        
        if not input_filename:
            input_filename = 'resultados.txt'
        
        try:
            statusfilter = [x.strip() for x in args[1].split('|')]
        except:
            statusfilter = []
        
        pagename = formatter.req.args.get('page', 'WikiStart')
        statusfilter = formatter.req.args.get('filter', ['P'])
        sections = []
        current_section = ('',[])
       
        try:
            attachment = Attachment(formatter.env, 'wiki', pagename, filename=input_filename, db=None)
        except:
            return format_to_html(self.env, formatter.context, 'Error al abrir: ' + input_filename)
            
        fd = attachment.open()
        data = fd.read()
        fd.close()

        if formatter.req.args.has_key("testcase") and id_ok:
            fout = StringIO.StringIO()

            for line in data.splitlines():
                parts = [x.strip('"').strip() for x in line.split(';')]
                if parts[0] == formatter.req.args.get ('testcase', ''):
                    testcase = to_unicode(parts[0]) #to_unicode(formatter.req.args.get ('testcase',''))
                    testname = to_unicode(parts[1]) #to_unicode(formatter.req.args.get ('testname',''))
                    testdescription = to_unicode(parts[2]) #(formatter.req.args.get ('testdescription',''))
                    teststatus = formatter.req.args.get('status', 'P')
                    testcomment = u" ".join(formatter.req.args.get ('comment', '').splitlines())
                    if len(testcomment) == 0:
                        testcomment = ''
                    strdata = "%s;%s;%s;%s;%s;%s;\n" % (testcase, testname, testdescription, teststatus, datetime.now().strftime('%d/%m/%y %H:%M:%S'), testcomment)
                    fout.write (strdata.encode('utf-8'))
                else:
                    fout.write(";".join(parts) + '\n')

            size = fout.tell()
            fout.seek(0)
            attachment.delete()
            
            attachment = Attachment(formatter.env, 'wiki', pagename)
            attachment.insert(input_filename, fout, size)
            data = fout.getvalue()        
            fout.close ()
            #No va bien la redirección...
            #formatter.req.redirect(formatter.req.href.wiki(pagename, filter=statusfilter))

        for line in data.splitlines():
            parts = [x.strip('"').strip() for x in line.split(';')]
            if parts[0].startswith('#'):
                if current_section[0] or len(current_section[1]) > 0:
                    sections.append(current_section)
                current_section = (to_unicode(parts[0][1:]), [])        
            elif len(parts) >= 3 and (parts[3] in statusfilter or parts[3] == ''):
                current_section[1].append ({
                    'testcase': to_unicode(parts[0]),
                    'testname': to_unicode(parts[1]),
                    'testdescription': to_unicode(parts[2]),
                    'status': to_unicode(parts[3]),
                    'teststamp': to_unicode(parts[4]),
                    'comment': to_unicode(parts[5])
                 })
            

        if current_section[0] or len(current_section[1]) > 0:
            sections.append(current_section)        
        
        add_stylesheet(formatter.req, 'tractests/css/testtable.css')
        #For Genshi, when used, return a 3 element tuple            
        return Chrome(self.env).render_template(formatter.req, 'testtable.html',
                                          {'cycle': itertools.cycle,
                                           'sections': sections,
                                           'statusfilter': statusfilter,
                                           'macro_id': macro_id},
                                        'text/html', False)


class CreateTestPageMacro(WikiMacroBase):
    """
    Creates a wikipage with an attachment containing a set of test specified in passed arguments.
    
    Arguments:
        CreateTestPage ([pages],[testpageprefix],[version],[filename],[foldername],[targetname])
        pages: a set of page (name of each one in trac) separated by '|'. If doesn't exist default value is current page
        testpageprefix: prefix to add at begin of new page name. If doesn`t exit default value is: test
        version: version of tests. If doesn't exist default value is: 1
        filename: name of file attached in source test page. If doesn't exist default value is: tests.txt
        foldername: new page is created inside it. If doesn't exist default value is: tests        
        targetname: name of file to attach in new page. If doesn't exist default value is: resultados.txt
    Example:
    {{{
        [[CreateTestPage]]
        [[CreateTestPage(SetPruebas/pruebas_no_regresivas|SetPruebas/pruebas_generales|SetPruebas/pruebas_especificas, pruebas, 2, setprueba.txt)]]
    }}}
    
    """        
    def expand_macro(self, formatter, name, input_args):
        
        pages = (formatter.req.args.get('page', 'WikiStart'),)
        filename = 'tests.txt'
        testpageprefix = 'test'
        foldername = 'tests'
        version = 1
        date = to_unicode(datetime.now().strftime('%y%m%d'))
        targetname = 'resultados.txt'
        
        try:
            macro_id = formatter.req.macro_id + 1
        except:
            macro_id = 0
        formatter.req.macro_id = macro_id
        
        try:
            id_ok = int(formatter.req.args.get('macro_id', None)) == macro_id
        except:
            id_ok = False        
         
        currpage = formatter.req.args.get('page', 'WikiStart')
        
        sections = []
        
        if input_args:
            argv = [arg.strip() for arg in input_args.split(',')]        

            if len(argv) >= 1 and argv[0]:
                pages = [pagestring.strip() for pagestring in argv[0].split('|')]
                
            if len(argv) >= 2 and argv[1]:
                testpageprefix = argv[1]
            if len(argv) >= 3 and argv[2]:
                version = argv[2]
            if len(argv) >= 4 and argv[3]:
                filename = argv[3]
            if len(argv) >= 5 and argv[4]:
                foldername = argv[4]
            if len(argv) >= 6 and argv[5]:
                targetname = argv[5]
  
        newpage = testpageprefix + "_" + date + u"_%03d" % int(version)
        fullname = foldername + u"/" + newpage
        page = WikiPage(formatter.env, fullname)

        for pagename in pages:
            
            tests = parse_tests_file(formatter.env,
                                     pagename,
                                     filename)
            
            sections.append(('File %s: %s' % (pagename, filename), tests, None))


        if formatter.req.args.get('createtestpage', None) and id_ok:
             
            page.text = u"||'''%s'''||version: '''%s'''||\n||Author: '''%s'''||date: '''%s'''|| \n" % (newpage, version, formatter.req.authname, datetime.now().strftime('%d/%m/%y %H:%M:%S'))
            page.text = page.text + u"[[ParseTestFile(%s)]] [[BR]] " % targetname
            
            fout = StringIO.StringIO()            
            fout.write(u"Id;Name;Description;Status;Date and Time;Comments;\n".encode('utf-8'))

            index = 1

            for section_title, tests, section_file in sections:

                for test in tests:
                    testid = newpage + u"_%03d" % index
                    if len(test) < 3 or parts[2] != 'S': #skip tests with status = 'S'
                        index = index + 1
                        strdata = u"%s;%s;%s;P;%s;;\n" % (testid, to_unicode(test[0]), to_unicode(test[1]), datetime.now().strftime('%d/%m/%y %H:%M:%S'))
                    else:
                        strdata = u";%s;%s;;;;\n" % (to_unicode(test[0]), to_unicode(test[1]))

                    fout.write(strdata.encode('utf-8'))

            fout.seek(0, 2) # seek to end of file
            size = fout.tell()

            fout.seek(0)

            page.save(formatter.req.authname, 'New test page from template' , formatter.req.remote_addr)

            attachmentchild = Attachment(formatter.env, 'wiki', fullname)
            attachmentchild.description = "test results"
            attachmentchild.author = formatter.req.authname   
            attachmentchild.insert('%s' % targetname, fout, size)

            fout.close() 
            
            formatter.req.redirect(formatter.req.href.wiki(page.name))

        wiki = WikiSystem(formatter.env)       
        pages_prefix = "%s/%s" % (foldername, testpageprefix)
        page_list = [{'url': formatter.req.href.wiki(p), 'name': p} for p in wiki.get_pages(pages_prefix)]
        return render_template_table(self.env,
                                       formatter.req,
                                       sections,
                                       'Preview for %s' % page.name, True,
                                       page_url= page.exists and formatter.req.href.wiki(page.name) or None,
                                       page_list=page_list)

class TestSuiteTemplateMacro(WikiMacroBase):
    """
    Defines a test suite template
    
    Example:
        !{{{
        #!TestSuiteTemplate
        !title General Tests
                
         Test 1 || Test description ||
         Test 2 || Description of test 2 ||
        
        !file OtherPage/moretests.txt
        
        !section Non-regresive Tests
         
         NR Test 1 || Description ||
         NR Test 2 || Description ||
         
        !section Others
        
        !file moretests.txt
         
        !include OtherTests
        !}}} 
    }}}
    
    """        
    def expand_macro(self, formatter, name, input_args):
        
        #Default title
        title = 'Test Suite'
        #List of sections, initially empty
        sections = []
        #Current section, no name, empty test list, related attachment
        current_section = (None, [], None)
        
        try:
            macro_id = formatter.req.macro_id + 1
        except:
            macro_id = 0
        formatter.req.macro_id = macro_id 

        try:
            id_ok = int(formatter.req.args.get('macro_id', None)) == macro_id
        except:
            id_ok = False
            
        currpage = formatter.req.args.get('page', 'WikiStart')
                    
        #Check if "New" test button was pressed
        if formatter.req.args.get('newtest', None) and id_ok:
            section_file = formatter.req.args.get('section_file', None)
                        
            fileparts = section_file.split("/")
            filename = fileparts[-1]
            if len(fileparts) > 1:
                filepage = "/".join(fileparts[:-1]) 
            else:
                filepage = currpage
            
            
            test_name = formatter.req.args.get('ntname', 'No name')
            test_desc = formatter.req.args.get('ntdesc', '')
            if test_name: 
                add_test_to_file(formatter.env, formatter.req,
                             filepage, filename,
                             [test_name, test_desc])
            formatter.req.redirect(formatter.req.href.wiki(currpage))
                        
        #Parse macro arguments, included pages and files to create test list
        for line in input_args.splitlines():
            
            line = line.strip()
            if line.startswith('!title '):
                #Parse title directive
                title = line[7:]
            elif line.startswith('!section '):
                #Parse new section
                if current_section[0] or len(current_section[1]) > 0:
                    sections.append(current_section)
                current_section = (line[9:], [], None)
            elif line.startswith('!include '):
                #TO-DO: Parse include directive
                #TO-DO: Add link to included page
                if current_section[0] or len(current_section[1]) > 0:
                    sections.append(current_section)                    
                sections.append(["Include %s: not yet implemented" % line[9:], [], None])
                current_section = (None, [], None)
                
            elif line.startswith('!file '):
                filepath = line[6:].strip()
                fileparts = filepath.split("/")
                filename = fileparts[-1]
                if len(fileparts) > 1:
                    filepage = "/".join(fileparts[:-1]) 
                else:
                    filepage = currpage
                    
                if current_section[0] or len(current_section[1]) > 0:
                    sections.append(current_section)

                tests = parse_tests_file(formatter.env,
                                         filepage,
                                         filename)
                
                sections.append([filename, tests, filepath])
                current_section = (None, [], None)
                
            elif line: #Skip blank lines
                #Parse test data, must be in form: Test Name || Test Description
                test_data = [x.strip() for x in line.split('||')]
                current_section[1].append(test_data)

        if current_section[0] or len(current_section[1]) > 0:
            sections.append(current_section)        
            
        #Check if Create Test Suite button is pressed
        newpage = formatter.req.args.get('suitename', None)
        if formatter.req.args.get('createsuite', None) and newpage and id_ok:
            page = WikiPage(formatter.env, newpage)    
            if not page.exists:
                create_tests_page(formatter.env, formatter.req, newpage, sections)
                #page.text = 'New page'            
                #page.save(formatter.req.authname, 'New test page from template' , formatter.req.remote_addr)
                
            formatter.req.redirect(formatter.req.href.wiki(page.name)) 
            
            
        return render_template_table(self.env, formatter.req, sections, title, False)            

