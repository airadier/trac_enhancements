"""
Copyright (C) 2009 Polar Technologies - www.polartech.es
Author: MAC <miguelangel.cuarteror@polartech.es>
"""
PACKAGE = 'tractests'
VERSION = '0.3'

from setuptools import setup

setup(
    name = 'tractests',
    version = VERSION,
    packages = ['tractests'],
    package_data={
        'tractests': [
            'htdocs/css/*.css',
            'templates/*.html'
        ]
    },
    author = "Miguel Angel Cuartero, Alvaro Iradier",
    author_email = "miguelangel.cuartero@polartech.es, alvaro.iradier@polartech.es",
    description = "Create and run sets of tests",
    license = "GPL",
    keywords = "trac plugin wiki test",
    url = "",

    entry_points = {
        'trac.plugins': [
            'tractests.tractests = tractests.tractests',
        ],
    },
    
    install_requires = [ 'Trac', ],
    
)
